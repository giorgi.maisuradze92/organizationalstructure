# Description
Task 1: 
    You can find api documentation  "localhost:5000/swagger"  
Task 2 : 
    Get - localhost:5000/api/Task2/{binaryString}

## How to run:
 docker-compose up

## Technologies:
* .Net core v2.2
* MongoDB 4.2.7  
* Docker
* MongoDB.Driver v2.11.1
* Swashbuckle.AspNetCore v4.0.1

### About:
This project was developed by [Giorgi Maisuradze]