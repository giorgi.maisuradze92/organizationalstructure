 
# Define base image
FROM microsoft/dotnet:2.2-sdk AS build-env

# Copy project files
WORKDIR /source
COPY ["OrganizationalStructureAPI/OrganizationalStructureAPI.csproj", "./OrganizationalStructureAPI/OrganizationalStructureAPI.csproj"]

# Restore
RUN dotnet restore "./OrganizationalStructureAPI/OrganizationalStructureAPI.csproj"

# Copy all source code
COPY . .

# Publish
WORKDIR /source
RUN dotnet publish -c Release -o /publish

# Runtime
FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /publish
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "OrganizationalStructureAPI.dll"]
