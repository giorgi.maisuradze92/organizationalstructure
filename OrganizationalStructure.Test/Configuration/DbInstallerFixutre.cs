﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using OrganizationalStructure.Core.DbContext;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrganizationalStructure.Test.Configuration
{
   public static class DbInstallerFixutre
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services, string connectionString, string databaseName)
        {
            ConventionRegistry.Register("Camel Case", new ConventionPack { new CamelCaseElementNameConvention() }, _ => true);
            services.AddSingleton<IMongoClient>(s => new MongoClient(connectionString));
            services.AddScoped(s => new ApplicationDbContext(s.GetRequiredService<IMongoClient>(), databaseName));

            services.AddScoped(c => c.GetService<IMongoClient>().StartSession());
            return services;
        }
    }
}
