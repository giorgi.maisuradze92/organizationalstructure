﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Newtonsoft.Json;
using OrganizationalStructure.Core.DbContext;
using OrganizationalStructure.Core.Models;
using OrganizationalStructure.Core.Utils;
using OrganizationalStructure.Core.ViewModels;
using OrganizationalStructureAPI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using OrganizationalStructure.Test.Configuration;
using OrganizationalStructure.Core.Repositories.Interfaces;
using OrganizationalStructure.Core.Repositories;
using OrganizationalStructure.Core.Services;
using OrganizationalStructure.Core.Services.Interfaces;

namespace OrganizationalStructure.Test.IntegrationTests
{
    public class ApiTest 
    {
        private readonly ITestOutputHelper _output;
        protected HttpClient _httpClient;
        private IServiceProvider _serviceProvider;
        public ApiTest(ITestOutputHelper output)
        {
            _output = output;
            InitializeEnvironmentForTests();

        }

        private void InitializeEnvironmentForTests()
        {
            var appFactory = new WebApplicationFactory<Startup>()
            .WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddDatabase("mongodb://admin:admin@localhost:27018", "Test"); 
                });
            });

            _httpClient = appFactory.CreateClient();
            _serviceProvider = appFactory.Server.Host.Services;
        }

        private Category GetCategory(string id)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                var category = db.Categories
                                 .Find(c => c.Id == id).FirstOrDefault();

                return category;
            }
        }



        [Fact]
        public async Task Create_ValidRequest_CategoryCreate()
        {
            var categoryRequest = new CreateCategoryRequest()
            {
                Name = "Giorgi",
                ParentId = null
            };

            var response = await _httpClient.PostAsync($"/api/categories",
              new StringContent(JsonConvert.SerializeObject(categoryRequest), Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var body = await response.BodyAs<CategoryObject>();

            Assert.NotNull(body);

            var category = GetCategory(body.Id);

            Assert.NotNull(category);
            Assert.True(category.Name == categoryRequest.Name);
        }


        public void Dispose()
        {
            //using (var scope = _serviceProvider.CreateScope())
            //{
            //    var db = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            //    var category = db.Categories.

            //    return category;
            //}
        }
    }
}
