﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OrganizationalStructure.Core.Utils
{
    public static class HttpResponseMessageExtensions
    {
        public static async Task<T> BodyAs<T>(this HttpResponseMessage httpResponseMessage)
        {
            var bodyString = await httpResponseMessage.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(bodyString);
        }
    }
}