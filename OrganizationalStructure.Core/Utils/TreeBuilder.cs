﻿using MongoDB.Bson;
using OrganizationalStructure.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrganizationalStructure.Core.Utils
{
    public static class TreeBuilder
    {
        public static IList<CategoryTree> BuildTree(this IEnumerable<CategoryTree> source)
        {

            var groups = source.GroupBy(i => i.ParentId);
            if (groups.Count() == 0)
                return source.ToList();
            var roots = groups.FirstOrDefault(g => g.Key.HasValue == false)?.ToList();

            if (roots == null)
            {
                return source.ToList();
            }


            if (roots.Count > 0)
            {
                var dict = groups.Where(g => g.Key.HasValue).ToDictionary(g => g.Key.Value, g => g.ToList());
                for (int i = 0; i < roots.Count; i++)
                    AddChildren(roots[i], dict);
            }

            return roots;
        }

        private static void AddChildren(CategoryTree node, IDictionary<ObjectId, List<CategoryTree>> source)
        {
            if (source.ContainsKey(node.Id))
            {
                node.Children = source[node.Id];
                for (int i = 0; i < node.Children.Count; i++)
                    AddChildren(node.Children[i], source);
            }
            else
            {
                node.Children = new List<CategoryTree>();
            }
        }
    }
}