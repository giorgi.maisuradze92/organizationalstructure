﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrganizationalStructure.Core.Models
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string ParentId { get; set; } 
    }
}
