﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OrganizationalStructure.Core.Models.Attributes
{
    public class BsonIdValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value == null || ObjectId.TryParse(value.ToString(), out ObjectId internalId);
        }
    }
}
