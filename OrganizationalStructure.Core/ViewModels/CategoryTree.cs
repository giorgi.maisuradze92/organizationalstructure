﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrganizationalStructure.Core.ViewModels
{
    public class CategoryTree
    {
        public CategoryTree()
        {
            Children = new List<CategoryTree>();
        }
        public ObjectId Id;
        public string Name { get; set; }
        public ObjectId? ParentId { get; set; }
        public List<CategoryTree> Children;
        public DateTime CreateDate { get; set; }

    }
}
