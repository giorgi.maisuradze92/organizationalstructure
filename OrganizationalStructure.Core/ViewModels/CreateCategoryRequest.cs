﻿using OrganizationalStructure.Core.Models.Attributes;
using System.ComponentModel.DataAnnotations;

namespace OrganizationalStructure.Core.ViewModels
{
    public class CreateCategoryRequest
    { 

        [Required]
        public string Name { get; set; }

        [BsonIdValidator(ErrorMessage = "ParentId must be BsonType.ObjectId")]
        public string ParentId { get; set; }
    }
}
