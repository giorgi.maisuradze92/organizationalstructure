﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrganizationalStructure.Core.ViewModels
{
    public class CategoryObject
    {
        public string Id { get; set; }
        public string Name { get; set; } 
        public string ParentId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
