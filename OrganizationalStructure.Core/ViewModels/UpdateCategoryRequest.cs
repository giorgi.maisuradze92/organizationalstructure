﻿using OrganizationalStructure.Core.Models.Attributes;
using System.ComponentModel.DataAnnotations;

namespace OrganizationalStructure.Core.ViewModels
{
    public class UpdateCategoryRequest
    {
        [Required]
        public string Name { get; set; }

        [BsonIdValidator(ErrorMessage = "ParentId must be BsonType.ObjectId or Null")]
        public string ParentId { get; set; }
    }
}
