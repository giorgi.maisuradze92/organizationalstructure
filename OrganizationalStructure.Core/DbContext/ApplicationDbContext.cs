﻿using MongoDB.Driver;
using OrganizationalStructure.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrganizationalStructure.Core.DbContext
{
    public class ApplicationDbContext
    {
        private readonly IMongoDatabase _database;
        public ApplicationDbContext(IMongoClient client, string dbName)
        {
            _database = client.GetDatabase(dbName);
        }

        public IMongoCollection<Category> Categories => _database.GetCollection<Category>("categories");
    }
}

