﻿using MongoDB.Bson;
using MongoDB.Driver;
using OrganizationalStructure.Core.Models;
using OrganizationalStructure.Core.Repositories.Interfaces;
using OrganizationalStructure.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrganizationalStructure.Core.Repositories
{

    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbContext.ApplicationDbContext dbContext, IClientSessionHandle session) : base(dbContext, session)
        {

        }


        public async Task<Category> GetCategoryAsync(string id)
        {
            var result = await _context.Categories
                                 .Find(c => c.Id == id)
                                 .FirstOrDefaultAsync();

            return result;
        }

        public async Task<Category> AddCategoryAsync(Category item)
        {

            await _context.Categories.InsertOneAsync(_session, item);
            return item;
        }

        public async Task<IEnumerable<CategoryTree>> GetChildrenAsync(string id = null)
        {
            var graphLookupStage = new BsonDocument("$graphLookup",
                        new BsonDocument("from", "categories")
                            .Add("startWith", "$_id")
                            .Add("connectFromField", "_id")
                            .Add("connectToField", "parentId")
                            .Add("as", "children"));


            var result = await _context.Categories.Aggregate()
                .Match(z => z.Id == id || id == null)
                .AppendStage<CategoryTree>(graphLookupStage).ToListAsync();

            return result;
        }

        public IEnumerable<Category> GetCategories()
        {
            return _context.Categories.AsQueryable();
        }

        public async Task DeleteAsync(string id)
        {
            await _context.Categories.DeleteOneAsync(_session, x => x.Id == id);
        }

        public async Task<IEnumerable<Category>> GetCategoriesByParentIdAsync(string id)
        {
            var result = await _context.Categories
                                .Find(c => c.ParentId == id).ToListAsync();

            return result;
        }

        public void UpdateCategory(Category category)
        {
            _context.Categories.ReplaceOne(x => x.Id == category.Id, category);
        }
    }
}
