﻿using OrganizationalStructure.Core.Models;
using OrganizationalStructure.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrganizationalStructure.Core.Repositories.Interfaces
{
    public interface ICategoryRepository
    {
        Task<Category> GetCategoryAsync(string id);

        Task<Category> AddCategoryAsync(Category item);

        IEnumerable<Category> GetCategories();

        Task<IEnumerable<CategoryTree>> GetChildrenAsync(string id = null);

        Task DeleteAsync(string id);

        Task<IEnumerable<Category>> GetCategoriesByParentIdAsync(string id);
        void UpdateCategory(Category category);
    }
}
