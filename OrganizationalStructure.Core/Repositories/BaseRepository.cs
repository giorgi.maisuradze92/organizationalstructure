﻿using MongoDB.Driver;
using OrganizationalStructure.Core.Models;
using OrganizationalStructure.Core.Repositories.Interfaces; 

namespace OrganizationalStructure.Core.Repositories
{
    public class BaseRepository<T> : IRepositoryBase<T> where T : BaseEntity
    {
        protected readonly IClientSessionHandle _session;
        protected readonly DbContext.ApplicationDbContext _context;
        public BaseRepository(DbContext.ApplicationDbContext dbContext, IClientSessionHandle session)
        {
            (_context, _session) = (dbContext, session);
        }
    }
}
