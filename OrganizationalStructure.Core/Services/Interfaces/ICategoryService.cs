﻿using OrganizationalStructure.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrganizationalStructure.Core.Services.Interfaces
{
    public interface ICategoryService
    {
        Task<CategoryObject> AddCategoryAsync(CreateCategoryRequest category);
        Task<CategoryObject> GetCategoryAsync(string id);
        IEnumerable<CategoryObject> GetCategories();
        Task<IEnumerable<CategoryTree>> GetChildrenAsync(string id = null);
        Task DeleteAsync(string id);
        void UpdateCategory(string id, CategoryObject old, UpdateCategoryRequest updated);
    }
}
