﻿using MongoDB.Driver;
using OrganizationalStructure.Core.Models;
using OrganizationalStructure.Core.Repositories.Interfaces;
using OrganizationalStructure.Core.Services.Interfaces;
using OrganizationalStructure.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrganizationalStructure.Core.Utils;

namespace OrganizationalStructure.Core.Services
{
    public class CategoryService : ICategoryService
    {

        private readonly ICategoryRepository _categoryRepository;
        private readonly IClientSessionHandle _session;
        public CategoryService(ICategoryRepository categoryRepository, IClientSessionHandle session)
        {
            (_categoryRepository, _session) = (categoryRepository, session);
        }
        public async Task<CategoryObject> AddCategoryAsync(CreateCategoryRequest category)
        {
            var result = await _categoryRepository.AddCategoryAsync(new Category()
            {
                Name = category.Name,
                ParentId = category.ParentId
            });

            return new CategoryObject()
            {
                Id = result.Id,
                Name = result.Name,
                ParentId = result.ParentId,
                CreateDate = result.CreateDate
            };
        }

        public async Task DeleteAsync(string id)
        {
            _session.StartTransaction();
            try
            {
                var childCategories = await _categoryRepository.GetCategoriesByParentIdAsync(id);
                await _categoryRepository.DeleteAsync(id);
                foreach (var item in childCategories)
                {
                    item.ParentId = null;
                    _categoryRepository.UpdateCategory(item);
                }

                _session.CommitTransaction();
            }
            catch
            {
                _session.AbortTransaction();
                throw;
            }
        }



        public IEnumerable<CategoryObject> GetCategories()
        {
            return _categoryRepository.GetCategories().Select(x => new CategoryObject()
            {
                Id = x.Id,
                Name = x.Name,
                ParentId = x.ParentId,
                CreateDate = x.CreateDate
            });
        }

        public async Task<CategoryObject> GetCategoryAsync(string id)
        {
            var result = await _categoryRepository.GetCategoryAsync(id);
            return result == null ? null : new CategoryObject()
            {
                Id = result.Id,
                Name = result.Name,
                CreateDate = result.CreateDate,
                ParentId = result.ParentId
            };
        }

        public async Task<IEnumerable<CategoryTree>> GetChildrenAsync(string id = null)
        {
            var result = await _categoryRepository.GetChildrenAsync(id);
            return result;
        }

        public void UpdateCategory(string id, CategoryObject old, UpdateCategoryRequest updated)
        {
            _categoryRepository.UpdateCategory(new Category()
            {
                Id = id,
                Name = updated.Name,
                ParentId = updated.ParentId,
                CreateDate = old.CreateDate
            });
        }
    }
}
