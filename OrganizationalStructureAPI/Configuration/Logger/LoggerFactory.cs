﻿using Microsoft.Extensions.Logging;

namespace OrganizationalStructureAPI.Configuration.Logger
{
    public static class LoggerFactory
    {
        public static ILoggerFactory ConfigureLoggerFactory(this ILoggerFactory log)
        {
            var projectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name.Split(".")[0].ToLower();

            var name = string.Format("logs/{0}", projectName);
            log.AddFile(name + "-{Date}.txt", LogLevel.Information);

            return log;
        }
    }
}
