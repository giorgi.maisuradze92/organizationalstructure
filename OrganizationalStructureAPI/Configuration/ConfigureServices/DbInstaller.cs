﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using OrganizationalStructure.Core.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrganizationalStructureAPI.Configuration.ConfigureServices
{
    public static class DbInstaller
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            ConventionRegistry.Register("Camel Case", new ConventionPack { new CamelCaseElementNameConvention() }, _ => true);
            services.AddSingleton<IMongoClient>(s => new MongoClient(configuration.GetConnectionString("MongoDb")));
            services.AddScoped(s => new ApplicationDbContext(s.GetRequiredService<IMongoClient>(), configuration["Database"]));

            services.AddScoped(c => c.GetService<IMongoClient>().StartSession());
            return services;
        }
    }
}
