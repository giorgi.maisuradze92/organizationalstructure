﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrganizationalStructure.Core.Services.Interfaces;
using OrganizationalStructure.Core.ViewModels;

namespace OrganizationalStructureAPI.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private ILogger<CategoryController> _logger;

        public CategoryController(ICategoryService categoryService, ILogger<CategoryController> logger)
        {
            _categoryService = categoryService;
            _logger = logger;
        }


        /// <summary>
        /// Returns category
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Category Id</param> 
        /// <response code="200">Returns specific Category</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<CategoryObject>> Get(string id)
        {
            try
            {

                var data = await _categoryService.GetCategoryAsync(id);
                if (data == null) return NoContent();

                return Ok(data);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        /// <summary>
        /// Returns category
        /// </summary>
        /// <returns></returns>
        /// <param name="id">Category Id</param> 
        /// <response code="200">Returns specific Category</response>
        [HttpGet("{id}/children")]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(403)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<IEnumerable<CategoryTree>>> GetChildrenAsync(string id)
        {
            try
            {

                var data = await _categoryService.GetCategoryAsync(id);
                if (data == null) return NoContent();
                return Ok(await _categoryService.GetChildrenAsync(id));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }



        /// <summary>
        /// Returns all Categories
        /// </summary>
        /// <returns></returns> 
        /// <response code="200">Returns all categories</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(403)]
        [ProducesResponseType(400)]
        [Produces("application/json")]
        public ActionResult<IEnumerable<CategoryObject>> Get()
        {
            try
            {
                return Ok(_categoryService.GetCategories());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }

        /// <summary>
        /// Returns all Categories Tree
        /// </summary>
        /// <returns></returns> 
        /// <response code="200">Returns all categories Tree</response>
        [HttpGet("tree")]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(403)]
        [ProducesResponseType(400)]
        [Produces("application/json")]
        public async Task<ActionResult<IEnumerable<CategoryObject>>> GetCategoryTreeAsync()
        {
            try
            {
                return Ok(await _categoryService.GetChildrenAsync());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }


        /// <summary>
        /// Create new Category
        /// </summary> 
        /// <response code="201"></response>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(403)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<CategoryObject>> PostAsync([FromBody] CreateCategoryRequest category)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                return StatusCode(StatusCodes.Status201Created, await _categoryService.AddCategoryAsync(category));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }


        /// <summary>
        /// Delete a Category
        /// </summary> 
        /// <response code="200"></response>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task DeleteAsync(string id)
        {
            try
            {
                await _categoryService.DeleteAsync(id);
                Response.StatusCode = 200;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                Response.StatusCode = 400;
            }
        }

        /// <summary>
        /// Update a Category
        /// </summary> 
        /// <response code="200"></response>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> UpdateAsync(string id, [FromBody] UpdateCategoryRequest category)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                var data = await _categoryService.GetCategoryAsync(id);
                if (data == null) return NoContent();
                _categoryService.UpdateCategory(id, data, category);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return BadRequest();
            }
        }
    }
}