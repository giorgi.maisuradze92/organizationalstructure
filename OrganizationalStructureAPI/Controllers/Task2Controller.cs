﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OrganizationalStructureAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Task2Controller : ControllerBase
    {


        /// <summary>
        /// Big O is linear 
        /// </summary> 
        [HttpGet("{binaryString}")]
        public bool Get(string binaryString)
        {
            int one = 0;
            int zero = 0;
            foreach (var item in binaryString)
            {
                if (item == '1')
                    one++;
                else
                    zero++;

                if (zero > one)
                    return false;
            }

            return one == zero;
        }
    }
}